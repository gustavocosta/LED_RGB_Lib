#include "main.h"

//LEDRGB led(14, 12, 13, COMMON_ANODE);
LEDRGB led(05, 04, 02, COMMON_CATHODE);

void setup() {
  Serial.begin(9600);
  //led.setRGBColor(0, 255, 255);
  led.setRGBColor(0xFF00FF);
  sum = 0;
  nextRead = millis();
  sel = 0;
  //led.blink(1500);
  //led.semaphore(3000, 2000, 3000);
  //led.rainbow(1, 100);
  led.fire();
}

void loop() {
  if (millis() > nextRead)
  {
    //led.randomColor();
    if(sel){
      led.fadeTo(255, 0, 0, 90, 3000);
      sel = 0;
    }else{
      led.fadeTo(0, 255, 255, 90, 3000);
      sel = 1;
    }
    //delay(2000);
    nextRead = nextRead + 5000;
  }
}
